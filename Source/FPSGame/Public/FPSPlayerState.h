// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/PlayerState.h"
#include "FPSPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class FPSGAME_API AFPSPlayerState : public APlayerState
{
	GENERATED_BODY()

	AFPSPlayerState();
	
public:

	UPROPERTY(Replicated, BlueprintReadWrite)
	FString TeamName;

    UPROPERTY(ReplicatedUsing=OnRep_Ready, BlueprintReadWrite)
    bool bIsReady;

	UPROPERTY(Replicated, BlueprintReadWrite)
	float DamageDone;

	UPROPERTY(Replicated, BlueprintReadWrite)
	float DamageTaken;
	
	virtual void BeginPlay() override;

	UFUNCTION()
	bool IsReady();

	UFUNCTION()
	void OnRep_Ready();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerChangeReady();

};
