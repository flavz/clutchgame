// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FPSGameModeHUD.generated.h"

/**
 * 
 */
UCLASS()
class FPSGAME_API UFPSGameModeHUD : public UUserWidget
{
	GENERATED_BODY()
	
	virtual void NativeConstruct() override;
	
	
};
