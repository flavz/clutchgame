// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerSphere.h"
#include "CaptureSphere.generated.h"


class USphereComponent;

UCLASS()
class FPSGAME_API ACaptureSphere : public ATriggerSphere
{
	GENERATED_BODY()
	
public:

	ACaptureSphere();

protected:

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Capture")
	float CaptureStatus;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Capture")
	float CaptureDefault;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Capture")
	float CaptureRateUp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Capture")
	float CaptureRateDown;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Capture")
	bool bCapture;

	UPROPERTY()
	FTimerHandle CaptureTimer;

public:

	UFUNCTION()
	void OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor);

	UFUNCTION()
	void OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor);
	
	UFUNCTION()
	void Capture();

	UFUNCTION()
	void UnCapture();

	UFUNCTION()
	bool GetCaptureStatus();

	UFUNCTION()
	void EndRound();

	
	
};
