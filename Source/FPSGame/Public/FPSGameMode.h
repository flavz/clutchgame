// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "FPSGameMode.generated.h"


class APlayerStart;

UCLASS()
class AFPSGameMode : public AGameMode
{
	GENERATED_BODY()

public:

	int32 MaxNumPlayers;

	UPROPERTY(BlueprintReadWrite)	
	TSubclassOf<class AFPSCharacter> bpAttacker;

	UPROPERTY(BlueprintReadWrite)
	TSubclassOf<class AFPSDefender> bpDefender;

	UPROPERTY()
	FTimerHandle RoundTimer;

	UPROPERTY()
	FTimerHandle RoundRestartTimer;

	UPROPERTY()
	FTimerHandle ReadyTimer;

	UPROPERTY(BlueprintReadWrite)
	float RoundTime;

	UPROPERTY()
	float RestartTime;
	
	UPROPERTY(BlueprintReadWrite)
	float FreezeTime;

	UPROPERTY()
	float ReadyNumber;

	UPROPERTY(BlueprintReadWrite)
	TArray<class APlayerController*> PlayerControllerList;

	TArray<AActor*> PlayerStarts;

	UPROPERTY()
	APlayerStart* PSAttacker;

	UPROPERTY()
	APlayerStart* PSDefender;

	AFPSGameMode();
	
	virtual void Tick(float DeltaSeconds);
	virtual void BeginPlay() override;
	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual bool ReadyToStartMatch_Implementation() override;


	UFUNCTION()
	void OnCapture();

	UFUNCTION()
	void ServerRoundTime();

	UFUNCTION()
	void ServerFreezeTime();

	UFUNCTION()
	void StartRound();

	UFUNCTION(BlueprintCallable)
	void RestartRound();

	UFUNCTION()
	void RespawnPlayers();

	UFUNCTION()
	void AddReady();

	UFUNCTION()
	void SubReady();

private:

	
};



