// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FPSCharacter.generated.h"


class UInputComponent;
class USkeletalMeshComponent;
class UCameraComponent;
class AFPSProjectile;
class USoundBase;
class UAnimSequence;
class UDamageType;
class UParticleSystem;


UCLASS()
class AFPSCharacter : public ACharacter
{
	GENERATED_BODY()

protected:

	/** Pawn mesh: 1st person view  */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Mesh")
	USkeletalMeshComponent* Mesh1PComponent;

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh")
	USkeletalMeshComponent* GunMeshComponent;

	/** Gun mesh: 3rt person view (seen only by self) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh")
	USkeletalMeshComponent* Gun3pMeshComponent;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
	UCameraComponent* CameraComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<UDamageType> DamageType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	USkeletalMeshComponent* MuzzleComponent;

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles strafing movement, left and right */
	void MoveRight(float Val);

	//UFUNCTION()
	//void OnHealthChanged(UHealthComponent* HealthComp, float Health, float HealthDelta, const class UDamageType* Damage, class AController* InstigatedBy, AActor* DamageCauser);

	// Player Pawn died prev
	UPROPERTY(BlueprintReadOnly, Category = "Player")
	bool bDied;

	

public:

	AFPSCharacter();

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category="Projectile")
	TSubclassOf<AFPSProjectile> ProjectileClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditDefaultsOnly, Category="Gameplay")
	USoundBase* FireSound;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditDefaultsOnly, Category = "Gameplay")
	UAnimSequence* FireAnimation;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadWrite, Category = "GamePlay")
	UParticleSystem* TracerEffect;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadWrite, Category = "GamePlay")
	UParticleSystem* TracerImpact;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Test")
	class UCapsuleComponent* WallDetectionR;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Test")
	class UCapsuleComponent* WallDetectionL;
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Movement")
	int32 MaxJumpCount;

	UPROPERTY()
	int32 CurrentJumpCount;

	UPROPERTY()
	bool WallJumpR;

	UPROPERTY()
	bool WallJumpL;

	UPROPERTY()
	int CallNumber;

	UPROPERTY()
	FTimerHandle Timer;

	UPROPERTY()
	FTimerHandle ReadyTimer;

	UPROPERTY(Replicated, BlueprintReadWrite)
	float TimeElapsed;

	UPROPERTY(Replicated, BlueprintReadWrite)
	float CurrentHealth;

	UPROPERTY(Replicated, BlueprintReadWrite)
	float MaxHealth;

	UPROPERTY(Replicated, BlueprintReadWrite)
	float bFire;

	UPROPERTY(Replicated, BlueprintReadWrite)
	FString TeamName;

	UFUNCTION()
	void RefireCheckTimer();

	UFUNCTION()
	void OnStartJump();

	UFUNCTION()
	void OnStopJump();

	void Fire();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerFire();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerOnStartJump();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerRefireCheckTimer();

	UFUNCTION(NetMultiCast, Reliable, WithValidation)
	void ServerTraceLine(FVector_NetQuantize TraceFrom, FVector_NetQuantize TraceEnd);

	UFUNCTION()
	void wallJump();

	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable)
	void SetReady();

	
	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	virtual bool CanJumpInternal_Implementation() const override;
	virtual void Landed(const FHitResult& Hit) override;

	/** Returns Mesh1P subobject **/
	USkeletalMeshComponent* GetMesh1P() const { return Mesh1PComponent; }

	/** Returns FirstPersonCameraComponent subobject **/
	UCameraComponent* GetFirstPersonCameraComponent() const { return CameraComponent; }

	

	
private:

	UFUNCTION()
	void OnOverlapBeginL(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnOverlapEndL(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void OnOverlapBeginR(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnOverlapEndR(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	
	
/*
	UPROPERTY(ReplicatedUsing=OnRep_HitScanTrace)
	FHitScanTrace HitScanTrace;

	UFUNCTION()
	void OnRep_HitScanTrace();
*/

};

