// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Net/UnrealNetwork.h"
#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "FPSGameState.generated.h"


/**
 * 
 */
UCLASS()
class FPSGAME_API AFPSGameState : public AGameState
{
	GENERATED_BODY()

public:
	AFPSGameState();

	UPROPERTY(Replicated, BlueprintReadWrite)
	float TeamAScore;

	UPROPERTY(Replicated, BlueprintReadWrite)
	float TeamBScore;
	
	UPROPERTY(Replicated, BlueprintReadWrite)
	float RoundTimeRep;

	UPROPERTY(Replicated, BlueprintReadWrite)
	float RoundRestartTimeRep;

	virtual void Tick(float Deltatime) override;
	virtual void BeginPlay() override;

	UFUNCTION()
	void AddScore(FString TeamName);
};
