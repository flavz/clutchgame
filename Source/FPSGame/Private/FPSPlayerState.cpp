// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSPlayerState.h"
#include "FPSGameMode.h"
#include "FPSGameState.h"



AFPSPlayerState::AFPSPlayerState() {


	TeamName = "Default";
	bIsReady = false;
	DamageDone = 0;
	DamageTaken = 0;

}

void AFPSPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {

	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AFPSPlayerState, TeamName);
	DOREPLIFETIME(AFPSPlayerState, DamageDone);
	DOREPLIFETIME(AFPSPlayerState, DamageTaken);
	DOREPLIFETIME(AFPSPlayerState, bIsReady);
    

}


void AFPSPlayerState::BeginPlay() {
	Super::BeginPlay();

	/*if (Role == ROLE_Authority) {
		AFPSGameState * GS = GetWorld()->GetGameState<AFPSGameState>();
		if (GS) {
			GS->PlayerArray.Add(this);
		}
	}*/
	
}

bool AFPSPlayerState::IsReady() {
	return bIsReady;
}


void AFPSPlayerState::OnRep_Ready() {
	ServerChangeReady();
}


void AFPSPlayerState::ServerChangeReady_Implementation() {

	AFPSGameMode* GM = Cast<AFPSGameMode>(GetWorld()->GetAuthGameMode());
	if (GM) {
		if (bIsReady == true) {
			GM->AddReady();
		}
		else {
			GM->SubReady();
		}
	}
		
	
}

bool AFPSPlayerState::ServerChangeReady_Validate() {
	return true;
}