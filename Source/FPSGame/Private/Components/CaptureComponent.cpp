// Fill out your copyright notice in the Description page of Project Settings.

#include "CaptureComponent.h"
#include "Components/SphereComponent.h"
#include "DrawDebugHelpers.h"


// Sets default values for this component's properties
UCaptureComponent::UCaptureComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	

}


// Called when the game starts
void UCaptureComponent::BeginPlay()
{
	Super::BeginPlay();

	CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(50.0f);
	CollisionComp->SetCollisionProfileName("Trigger");
	CollisionComp->OnComponentBeginOverlap.AddDynamic(this, &UCaptureComponent::OnOverlapBegin);
	CollisionComp->OnComponentEndOverlap.AddDynamic(this, &UCaptureComponent::OnOverlapEnd);	
}


void UCaptureComponent::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor) {
		
	}
}

void UCaptureComponent::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor) {
		
	}
}

