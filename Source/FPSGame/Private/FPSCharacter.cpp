// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "FPSCharacter.h"
#include "FPSProjectile.h"
#include "FPSPlayerState.h"
#include "FPSGameMode.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "FPSGame.h"
#include "HealthComponent.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "Net/UnrealNetwork.h"

AFPSCharacter::AFPSCharacter()
{
	// Create a CameraComponent	
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	CameraComponent->SetupAttachment(GetCapsuleComponent());
	CameraComponent->RelativeLocation = FVector(0, 0, BaseEyeHeight); // Position the camera
	CameraComponent->bUsePawnControlRotation = true;

	// Create a wall detection compinent
	WallDetectionR = CreateDefaultSubobject<UCapsuleComponent>(TEXT("WallDetectionR"));
	WallDetectionR->InitCapsuleSize(80.f, 110.0f);
	WallDetectionR->SetupAttachment(GetCapsuleComponent());
	WallDetectionR->BodyInstance.SetCollisionProfileName("Trigger");
	WallDetectionR->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	WallDetectionL = CreateDefaultSubobject<UCapsuleComponent>(TEXT("WallDetectionL"));
	WallDetectionL->InitCapsuleSize(80.f, 110.0f);
	WallDetectionL->SetupAttachment(GetCapsuleComponent());
	WallDetectionL->BodyInstance.SetCollisionProfileName("Trigger");
	WallDetectionL->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	WallDetectionR->OnComponentBeginOverlap.AddDynamic(this, &AFPSCharacter::OnOverlapBeginR);
	WallDetectionR->OnComponentEndOverlap.AddDynamic(this, &AFPSCharacter::OnOverlapEndR);

	WallDetectionL->OnComponentBeginOverlap.AddDynamic(this, &AFPSCharacter::OnOverlapBeginL);
	WallDetectionL->OnComponentEndOverlap.AddDynamic(this, &AFPSCharacter::OnOverlapEndL);


	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1PComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh"));
	Mesh1PComponent->SetupAttachment(CameraComponent);
	Mesh1PComponent->CastShadow = false;
	Mesh1PComponent->RelativeRotation = FRotator(2.0f, -15.0f, 5.0f);
	Mesh1PComponent->RelativeLocation = FVector(0, 0, -160.0f);

	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECR_Ignore);

	// Create a gun mesh component
	GunMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	GunMeshComponent->CastShadow = false;
	GunMeshComponent->SetupAttachment(Mesh1PComponent, "GripPoint");

	
	

	MuzzleComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Muzzle"));
	MuzzleComponent->SetupAttachment(CameraComponent);
	
	WallJumpR = false;
	WallJumpL = false;
	CallNumber = 0;
	CurrentJumpCount = 0;
	MaxJumpCount = 3;

	MaxHealth = 3.0f;
	CurrentHealth = 3.0f;

	TimeElapsed = 3.0f;
	bFire = 1;
	TeamName = "Attacker";

}


void AFPSCharacter::BeginPlay() {
	Super::BeginPlay();

}

void AFPSCharacter::Tick(float DeltaSeconds) {
	Super::Tick(DeltaSeconds);

}
	



void AFPSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AFPSCharacter::OnStartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AFPSCharacter::OnStopJump);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AFPSCharacter::Fire);

	PlayerInputComponent->BindAxis("MoveForward", this, &AFPSCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFPSCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
}


void AFPSCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
	
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AFPSCharacter, CurrentHealth);
	DOREPLIFETIME(AFPSCharacter, TimeElapsed);
	DOREPLIFETIME(AFPSCharacter, bFire);
	DOREPLIFETIME(AFPSCharacter, TeamName);
}


float AFPSCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser ) {

	float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	CurrentHealth -= ActualDamage;

	UE_LOG(LogTemp, Log, TEXT("HEALTH --------->  %s"), *FString::SanitizeFloat(CurrentHealth));

	if (CurrentHealth <= 0) {

		GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		DetachFromControllerPendingDestroy();

		SetLifeSpan(1.0f);
		//Destroy();
	}

	return ActualDamage;
}



void AFPSCharacter::Fire()
{
	if (bFire >= 1) {
		bFire = 0;
		ServerFire();


		// try and play the sound if specified
		/*if (FireSound)
		{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
		}*/

		// try and play a firing animation if specified
		if (FireAnimation)
		{
			// Get the animation object for the arms mesh
			UAnimInstance* AnimInstance = Mesh1PComponent->GetAnimInstance();
			if (AnimInstance)
			{
				AnimInstance->PlaySlotAnimationAsDynamicMontage(FireAnimation, "Arms", 0.0f);
			}
		}
		
		CallNumber = 100;
		TimeElapsed = 0;

		GetWorldTimerManager().SetTimer(Timer, this, &AFPSCharacter::RefireCheckTimer, 0.03f, true, 0.0f);
	}
}



void AFPSCharacter::ServerFire_Implementation() {

	// try and fire a projectile
	if (ProjectileClass)
	{
		
		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Orange, FString::FString("FIRE!"));
		FVector MuzzleLocation = GunMeshComponent->GetSocketLocation("Muzzle");
		FRotator MuzzleRotation = GunMeshComponent->GetSocketRotation("Muzzle");

		//Set Spawn Collision Handling Override
		FActorSpawnParameters ActorSpawnParams;
		ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		ActorSpawnParams.Owner = this;



		// Trace the world, from pawn eyes to crosshair location
		FHitResult Hit;

		FCollisionQueryParams QueryParams;
		QueryParams.AddIgnoredActor(this);
		QueryParams.bTraceAsyncScene = true;
		QueryParams.bTraceComplex = true;

		FVector EyeLocation;
		FRotator EyeRotation;
		this->GetActorEyesViewPoint(EyeLocation, EyeRotation);

		FVector ShotDirection = EyeRotation.Vector();
		FVector TraceEnd = EyeLocation + (ShotDirection * 10000);

		if (GetWorld()->LineTraceSingleByChannel(Hit, EyeLocation, TraceEnd, ECC_Visibility, QueryParams)) {

			// THIS IS FOR ATTACKER
			AActor* HitActor = Hit.GetActor();
			UGameplayStatics::ApplyPointDamage(HitActor, 1.0f, ShotDirection, Hit, this->GetInstigatorController(), this, DamageType);
		}

		UParticleSystemComponent* TracerComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TracerEffect, EyeLocation);
		if (TracerComp) {
			TracerComp->SetVectorParameter("Target", TraceEnd);
		}

		ServerTraceLine(EyeLocation, Hit.ImpactPoint);
	}
}


bool AFPSCharacter::ServerFire_Validate() {

	return true;
}


void AFPSCharacter::ServerTraceLine_Implementation(FVector_NetQuantize TraceFrom, FVector_NetQuantize TraceEnd) {
	
	UParticleSystemComponent* TracerComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TracerEffect, TraceFrom);
	if (TracerComp) {
		TracerComp->SetVectorParameter("Target", TraceEnd);
	}
	UParticleSystemComponent* TracerImpactComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TracerImpact, TraceEnd);
}

bool AFPSCharacter::ServerTraceLine_Validate(FVector_NetQuantize TraceFrom, FVector_NetQuantize TraceEnd) {
	return true;
}


void AFPSCharacter::RefireCheckTimer() {

	bFire += 0.01;
	if (bFire >= 1) {
		GetWorldTimerManager().ClearTimer(Timer);
	}
}

void AFPSCharacter::ServerRefireCheckTimer_Implementation() {

	bFire += 1;

}

bool AFPSCharacter::ServerRefireCheckTimer_Validate() {
	return true;
}



bool AFPSCharacter::CanJumpInternal_Implementation() const {

	return Super::CanJumpInternal_Implementation() || (CurrentJumpCount > 0 && CurrentJumpCount < MaxJumpCount) && (WallJumpR || WallJumpL);
}

void AFPSCharacter::OnStartJump() {

	if (CurrentJumpCount <= 0) {
		bPressedJump = true;
	}
	else {
		ServerOnStartJump();
	}
	CurrentJumpCount++;
}

void AFPSCharacter::ServerOnStartJump_Implementation() {

	if (WallJumpL) {
		this->LaunchCharacter((GetActorRightVector() * 300) + (GetActorUpVector() * 300), false, false);
	}
	else if (WallJumpR) {
		this->LaunchCharacter((GetActorRightVector() * -300) + (GetActorUpVector() * 300), false, false);
	}

}

bool AFPSCharacter::ServerOnStartJump_Validate() {

	return true;
}



void AFPSCharacter::wallJump() {

	if (WallJumpL && CurrentJumpCount > 0 ) {
		this->LaunchCharacter((GetActorRightVector() * 300) + (GetActorUpVector() * 300), false, false);
	}
	else if (WallJumpR && CurrentJumpCount > 0) {
		this->LaunchCharacter((GetActorRightVector() * -300) + (GetActorUpVector() * 300), false, false);
	}
}



void AFPSCharacter::OnStopJump() {
	bPressedJump = false;
}

void AFPSCharacter::Landed(const FHitResult& Hit)
{
	CurrentJumpCount = 0;
}

void AFPSCharacter::OnOverlapBeginL(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {

	if (OtherActor != this) 
		WallJumpL = true;
}

void AFPSCharacter::OnOverlapEndL(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {

	if (OtherActor != this) {
		WallJumpL = false;
		CurrentJumpCount = 0;	
	}
}

void AFPSCharacter::OnOverlapBeginR(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {

	if (OtherActor != this) 
		WallJumpR = true;
}

void AFPSCharacter::OnOverlapEndR(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {

	if (OtherActor != this) {
		WallJumpR = false;
		CurrentJumpCount = 0;
	}
}


void AFPSCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}


void AFPSCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AFPSCharacter::SetReady_Implementation() {

	
	AFPSPlayerState * ps = Cast<AFPSPlayerState>(PlayerState);
	if (ps) {
		if (ps->IsReady()) {
			ps->bIsReady = false;
		}
		else {
			ps->bIsReady = true;
		}
	}
}

bool AFPSCharacter::SetReady_Validate() {
	return true;
}