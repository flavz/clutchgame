// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSDefender.h"
#include "FPSProjectile.h"
#include "FPSPlayerState.h"
#include "FPSGameMode.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "FPSGame.h"
#include "HealthComponent.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "Net/UnrealNetwork.h"

AFPSDefender::AFPSDefender()
{
	// Create a CameraComponent	
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	CameraComponent->SetupAttachment(GetCapsuleComponent());
	CameraComponent->RelativeLocation = FVector(0, 0, BaseEyeHeight); // Position the camera
	CameraComponent->bUsePawnControlRotation = true;

	// Create a wall detection compinent
	WallDetectionR = CreateDefaultSubobject<UCapsuleComponent>(TEXT("WallDetectionR"));
	WallDetectionR->InitCapsuleSize(80.f, 110.0f);
	WallDetectionR->SetupAttachment(GetCapsuleComponent());
	WallDetectionR->BodyInstance.SetCollisionProfileName("Trigger");
	WallDetectionR->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	WallDetectionL = CreateDefaultSubobject<UCapsuleComponent>(TEXT("WallDetectionL"));
	WallDetectionL->InitCapsuleSize(80.f, 110.0f);
	WallDetectionL->SetupAttachment(GetCapsuleComponent());
	WallDetectionL->BodyInstance.SetCollisionProfileName("Trigger");
	WallDetectionL->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	WallDetectionR->OnComponentBeginOverlap.AddDynamic(this, &AFPSDefender::OnOverlapBeginR);
	WallDetectionR->OnComponentEndOverlap.AddDynamic(this, &AFPSDefender::OnOverlapEndR);

	WallDetectionL->OnComponentBeginOverlap.AddDynamic(this, &AFPSDefender::OnOverlapBeginL);
	WallDetectionL->OnComponentEndOverlap.AddDynamic(this, &AFPSDefender::OnOverlapEndL);


	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1PComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh"));
	Mesh1PComponent->SetupAttachment(CameraComponent);
	Mesh1PComponent->CastShadow = false;
	Mesh1PComponent->RelativeRotation = FRotator(2.0f, -15.0f, 5.0f);
	Mesh1PComponent->RelativeLocation = FVector(0, 0, -160.0f);

	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECR_Ignore);

	// Create a gun mesh component
	GunMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	GunMeshComponent->CastShadow = false;
	GunMeshComponent->SetupAttachment(Mesh1PComponent, "GripPoint");




	MuzzleComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Muzzle"));
	MuzzleComponent->SetupAttachment(CameraComponent);

	WallJumpR = false;
	WallJumpL = false;
	CallNumber = 0;
	CurrentJumpCount = 0;
	MaxJumpCount = 3;

	MaxHealth = 1.0f;
	CurrentHealth = 1.0f;

	TimeElapsed = 3.0f;
	bFire = 1;
	Ammo = 4;
	Tags.Add(FName("Defender"));
}

void AFPSDefender::BeginPlay() {
	Super::BeginPlay();

	//HealthComp->OnHealthChanged.AddDynamic(this, &AFPSDefender::OnHealthChanged);
}

void AFPSDefender::Tick(float DeltaSeconds) {
	Super::Tick(DeltaSeconds);

}




void AFPSDefender::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AFPSDefender::OnStartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AFPSDefender::OnStopJump);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AFPSDefender::Fire);

	PlayerInputComponent->BindAxis("MoveForward", this, &AFPSDefender::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFPSDefender::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
}


void AFPSDefender::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {

	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AFPSDefender, CurrentHealth);
	DOREPLIFETIME(AFPSDefender, TimeElapsed);
	DOREPLIFETIME(AFPSDefender, bFire);
	DOREPLIFETIME(AFPSDefender, TeamName);
}


float AFPSDefender::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) {

	float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	CurrentHealth -= ActualDamage;

	UE_LOG(LogTemp, Log, TEXT("HEALTH --------->  %s"), *FString::SanitizeFloat(CurrentHealth));

	if (CurrentHealth <= 0) {

		GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		DetachFromControllerPendingDestroy();

		SetLifeSpan(1.0f);
		//Destroy();
	}

	return ActualDamage;
}



void AFPSDefender::Fire()
{
	if (bFire >= 0.25) {
		bFire -= 0.25;
		ServerFire();


		// try and play the sound if specified
		/*if (FireSound)
		{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
		}*/

		// try and play a firing animation if specified
		if (FireAnimation)
		{
			// Get the animation object for the arms mesh
			UAnimInstance* AnimInstance = Mesh1PComponent->GetAnimInstance();
			if (AnimInstance)
			{
				AnimInstance->PlaySlotAnimationAsDynamicMontage(FireAnimation, "Arms", 0.0f);
			}
		}

		CallNumber = 100;
		TimeElapsed = 0;

		GetWorldTimerManager().SetTimer(Timer, this, &AFPSDefender::RefireCheckTimer, 0.06f, true, 0.0f);
	}
}



void AFPSDefender::ServerFire_Implementation() {

	// try and fire a projectile
	if (ProjectileClass)
	{

		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Orange, FString::FString("FIRE!"));
		FVector MuzzleLocation = GunMeshComponent->GetSocketLocation("Muzzle");
		FRotator MuzzleRotation = GunMeshComponent->GetSocketRotation("Muzzle");

		//Set Spawn Collision Handling Override
		FActorSpawnParameters ActorSpawnParams;
		ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		ActorSpawnParams.Owner = this;



		// Trace the world, from pawn eyes to crosshair location
		FHitResult Hit;

		FCollisionQueryParams QueryParams;
		QueryParams.AddIgnoredActor(this);
		QueryParams.bTraceAsyncScene = true;
		QueryParams.bTraceComplex = true;

		FVector EyeLocation;
		FRotator EyeRotation;
		this->GetActorEyesViewPoint(EyeLocation, EyeRotation);

		FVector ShotDirection = EyeRotation.Vector();
		FVector TraceEnd = EyeLocation + (ShotDirection * 10000);

		if (GetWorld()->LineTraceSingleByChannel(Hit, EyeLocation, TraceEnd, ECC_Visibility, QueryParams)) {

			// THIS IS FOR ATTACKER
			//AActor* HitActor = Hit.GetActor();
			//UGameplayStatics::ApplyPointDamage(HitActor, 1.0f, ShotDirection, Hit, this->GetInstigatorController(), this, DamageType);
			
		}

		GetWorld()->SpawnActor<AFPSProjectile>(ProjectileClass, EyeLocation, EyeRotation, ActorSpawnParams);

		UParticleSystemComponent* TracerComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TracerEffect, EyeLocation);
		if (TracerComp) {
			TracerComp->SetVectorParameter("Target", TraceEnd);
		}
		




		//ServerTraceLine(EyeLocation, Hit.ImpactPoint);



		// THIS IS FOR DEFENDER
		

	}
}


bool AFPSDefender::ServerFire_Validate() {

	return true;
}


void AFPSDefender::ServerTraceLine_Implementation(FVector_NetQuantize TraceFrom, FVector_NetQuantize TraceEnd) {

	UParticleSystemComponent* TracerComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TracerEffect, TraceFrom);
	if (TracerComp) {
		TracerComp->SetVectorParameter("Target", TraceEnd);
	}
	UParticleSystemComponent* TracerImpactComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TracerImpact, TraceEnd);
}

bool AFPSDefender::ServerTraceLine_Validate(FVector_NetQuantize TraceFrom, FVector_NetQuantize TraceEnd) {
	return true;
}


void AFPSDefender::RefireCheckTimer() {

	bFire += 0.01;
	if (bFire >= 1) {
		GetWorldTimerManager().ClearTimer(Timer);
	}
}

void AFPSDefender::ServerRefireCheckTimer_Implementation() {

	bFire += 1;

}

bool AFPSDefender::ServerRefireCheckTimer_Validate() {
	return true;
}



bool AFPSDefender::CanJumpInternal_Implementation() const {

	return Super::CanJumpInternal_Implementation() || (CurrentJumpCount > 0 && CurrentJumpCount < MaxJumpCount) && (WallJumpR || WallJumpL);
}

void AFPSDefender::OnStartJump() {

	if (CurrentJumpCount <= 0) {
		bPressedJump = true;
	}
	else {
		ServerOnStartJump();
	}
	CurrentJumpCount++;
}

void AFPSDefender::ServerOnStartJump_Implementation() {

	if (WallJumpL) {
		this->LaunchCharacter((GetActorRightVector() * 300) + (GetActorUpVector() * 300), false, false);
	}
	else if (WallJumpR) {
		this->LaunchCharacter((GetActorRightVector() * -300) + (GetActorUpVector() * 300), false, false);
	}

}

bool AFPSDefender::ServerOnStartJump_Validate() {

	return true;
}



void AFPSDefender::wallJump() {

	if (WallJumpL && CurrentJumpCount > 0) {
		this->LaunchCharacter((GetActorRightVector() * 300) + (GetActorUpVector() * 300), false, false);
	}
	else if (WallJumpR && CurrentJumpCount > 0) {
		this->LaunchCharacter((GetActorRightVector() * -300) + (GetActorUpVector() * 300), false, false);
	}
}



void AFPSDefender::OnStopJump() {
	bPressedJump = false;
}

void AFPSDefender::Landed(const FHitResult& Hit)
{
	CurrentJumpCount = 0;
}

void AFPSDefender::OnOverlapBeginL(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {

	if (OtherActor != this)
		WallJumpL = true;
}

void AFPSDefender::OnOverlapEndL(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {

	if (OtherActor != this) {
		WallJumpL = false;
		CurrentJumpCount = 0;
	}
}

void AFPSDefender::OnOverlapBeginR(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {

	if (OtherActor != this)
		WallJumpR = true;
}

void AFPSDefender::OnOverlapEndR(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {

	if (OtherActor != this) {
		WallJumpR = false;
		CurrentJumpCount = 0;
	}
}


void AFPSDefender::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}


void AFPSDefender::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AFPSDefender::SetReady_Implementation() {


	AFPSPlayerState * ps = Cast<AFPSPlayerState>(PlayerState);
	if (ps) {
		if (ps->IsReady()) {
			ps->bIsReady = false;
		}
		else {
			ps->bIsReady = true;
		}
	}
}

bool AFPSDefender::SetReady_Validate() {
	return true;
}