// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "FPSGameMode.h"
#include "FPSGameState.h"
#include "FPSHUD.h"
#include "FPSCharacter.h"
#include "FPSDefender.h"
#include "FPSPlayerState.h"
#include "UObject/ConstructorHelpers.h"
#include "FPSGame.h"
#include "GameFramework/GameMode.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"


AFPSGameMode::AFPSGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprints/Bug"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	static ConstructorHelpers::FObjectFinder<UClass> bpAttackerClassFinder(TEXT("/Game/Blueprints/BP_Player.BP_Player_C"));
	bpAttacker = bpAttackerClassFinder.Object;

	static ConstructorHelpers::FObjectFinder<UClass> bpDefenderClassFinder(TEXT("/Game/Blueprints/BP_Defender.BP_Defender_C"));
	bpDefender = bpDefenderClassFinder.Object;

	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerFinder(TEXT("/Game/Blueprints/BP_PlayerController"));
	PlayerControllerClass = PlayerControllerFinder.Class;

	static ConstructorHelpers::FClassFinder<APlayerState> PlayerStateClassFinder(TEXT("/Game/Blueprints/BP_PlayerState"));
	PlayerStateClass = PlayerStateClassFinder.Class;

	static ConstructorHelpers::FClassFinder<AGameState> GameStateFinder(TEXT("/Game/Blueprints/BP_GameState"));
	GameStateClass = GameStateFinder.Class;

	// use our custom HUD class
	HUDClass = AFPSHUD::StaticClass();
	//PlayerStateClass = AFPSPlayerState::StaticClass();
	
	MaxNumPlayers = 2;
	RestartTime = 0;
	RoundTime = 10;
	FreezeTime = 5;
	ReadyNumber = 0;

}



void AFPSGameMode::Tick(float DeltaSeconds) {
	Super::Tick(DeltaSeconds);

}


void AFPSGameMode::BeginPlay() {
	Super::BeginPlay();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerStart::StaticClass(), PlayerStarts);
	RestartRound();
	
}

void AFPSGameMode::PostLogin(APlayerController* NewPlayer) {
	
	Super::PostLogin(NewPlayer);

	this->PlayerControllerList.Add(NewPlayer);
	//NewPlayer->InitPlayerState();


	UE_LOG(LogTemp, Log, TEXT("Player Connected : %s"), *NewPlayer->GetName());

	if(NumPlayers >= 2) {
		RestartRound();
	}
}




bool AFPSGameMode::ReadyToStartMatch_Implementation() {
	
	if (ReadyNumber == NumPlayers && NumPlayers >= 2) {
		UE_LOG(LogTemp, Log, TEXT("STARTING MATCH"));
		return true;
	}
	return false;
}

void AFPSGameMode::RestartRound() {

	RestartTime = 6;
	GetWorldTimerManager().SetTimer(RoundRestartTimer, this, &AFPSGameMode::StartRound, 1.0f, true, 0.0f);

}


void AFPSGameMode::StartRound() {
	
	RestartTime--;
	AFPSGameState* GS = GetWorld()->GetGameState<AFPSGameState>();
	if (GS) {
		GS->RoundRestartTimeRep = RestartTime;
	}
	

	UE_LOG(LogTemp, Log, TEXT("RESTART MATCH------------> %s"), *FString::SanitizeFloat(RestartTime));

	if (RestartTime <= 0) {
		FreezeTime = 5;
		RespawnPlayers();
		GetWorldTimerManager().ClearTimer(RoundRestartTimer);
		GetWorldTimerManager().SetTimer(RoundTimer, this, &AFPSGameMode::ServerFreezeTime, 1.0f, true, 0.0f);
	}
	
}


void AFPSGameMode::ServerFreezeTime() {

	FreezeTime--;
	AFPSGameState* GS = GetWorld()->GetGameState<AFPSGameState>();
	if (GS) {
		GS->RoundTimeRep = FreezeTime;
	}

	if (FreezeTime <= 0) {
		RoundTime = 100;
		GetWorldTimerManager().ClearTimer(RoundTimer);
		GetWorldTimerManager().SetTimer(RoundTimer, this, &AFPSGameMode::ServerRoundTime, 1.0f, true, 0.0f);
	}
}

void AFPSGameMode::ServerRoundTime() {
	
	RoundTime--;
	AFPSGameState* GS = GetWorld()->GetGameState<AFPSGameState>();
	if (GS) {
		GS->RoundTimeRep = RoundTime;
	}
	

	if (RoundTime <= 0) {
		GetWorldTimerManager().ClearTimer(RoundTimer);
		GS->AddScore("Defender");
		RestartRound();
	}
	
}


void AFPSGameMode::RespawnPlayers() {

	for (int i = 0; i < PlayerControllerList.Num(); i++) {
		if (PlayerControllerList[i]->GetCharacter() != NULL) {
			PlayerControllerList[i]->GetCharacter()->Destroy();
		}
	}

	for (int i = 0; i < PlayerStarts.Num(); i++) {
		
		APlayerStart * CastedPlayerStart = Cast<APlayerStart>(PlayerStarts[i]);

		if (CastedPlayerStart->PlayerStartTag == "Attacker") {
			UE_LOG(LogTemp, Log, TEXT("PSAttacker ----------->"));
			PSAttacker = CastedPlayerStart;
		}
		else if (CastedPlayerStart->PlayerStartTag == "Defender") {
			UE_LOG(LogTemp, Log, TEXT("PSDefender ----------->"));
			PSDefender = CastedPlayerStart;
		}
	}

	FVector AttackerSpawnLocation = PSAttacker->GetActorLocation();
	FRotator AttackerSpawnRotation = PSAttacker->GetActorRotation();

	FVector DefenderSpawnLocation = PSDefender->GetActorLocation();
	FRotator DefenderSpawnRotation = PSDefender->GetActorRotation();
	//UE_LOG(LogTemp, Log, TEXT("LOCATION PLAYER START -----------> %s"), *PlayerStarts[0]->GetActorLocation().ToString());
			
	FActorSpawnParameters params;
	UWorld* world = GetWorld();
	AFPSCharacter * a1 = world->SpawnActor<AFPSCharacter>(bpAttacker, AttackerSpawnLocation, AttackerSpawnRotation, params);
	AFPSDefender * d1 = world->SpawnActor<AFPSDefender>(bpDefender, DefenderSpawnLocation, DefenderSpawnRotation, params);

	PlayerControllerList[0]->Possess(a1);
	PlayerControllerList[1]->Possess(d1);

}

void AFPSGameMode::AddReady() {
	ReadyNumber++;
	UE_LOG(LogTemp, Log, TEXT("AddReady -----------> %s"), *FString::SanitizeFloat(ReadyNumber));

	AFPSGameState * GS = Cast<AFPSGameState>(GetWorld()->GetGameState());
	if (GS) {
		UE_LOG(LogTemp, Log, TEXT("MATCHSTATE -----------> %s"), *GS->GetMatchState().ToString());
	}

	if(NumPlayers == ReadyNumber) {
		StartMatch();
		AFPSGameState * GS2 = Cast<AFPSGameState>(GetWorld()->GetGameState());
		if(GS2) {
			UE_LOG(LogTemp, Log, TEXT("MATCHSTATE -----------> %s"), *GS2->GetMatchState().ToString());
		}

		RestartRound();
	}
}

void AFPSGameMode::SubReady() {
	ReadyNumber--;
	UE_LOG(LogTemp, Log, TEXT("SubReady -----------> %s"), *FString::SanitizeFloat(ReadyNumber));

}


void AFPSGameMode::OnCapture() {

	GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Cyan, FString::FString("ENDGAME!"));
	AFPSGameState* GS = GetWorld()->GetGameState<AFPSGameState>();
	GS->AddScore("Attacker");

	RestartRound();

}