// Fill out your copyright notice in the Description page of Project Settings.

#include "CaptureSphere.h"
#include "FPSGame.h"
#include "FPSGameMode.h"
#include "FPSCharacter.h"
#include "Net/UnrealNetwork.h"

ACaptureSphere::ACaptureSphere() {

	CaptureStatus = 0.0f;
	CaptureDefault = 1000.0f;
	CaptureRateUp = 0.01f;
	CaptureRateDown = 0.02f;
	bCapture = false;

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

}


void ACaptureSphere::BeginPlay()
{
	Super::BeginPlay();

	OnActorBeginOverlap.AddDynamic(this, &ACaptureSphere::OnOverlapBegin);
	OnActorEndOverlap.AddDynamic(this, &ACaptureSphere::OnOverlapEnd);
}

void ACaptureSphere::Tick(float DeltaSeconds) {

	Super::Tick(DeltaSeconds);

	if(bCapture) {
		EndRound();
		bCapture = false;
	}
}

void ACaptureSphere::OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor) {


	AFPSCharacter * attacker = Cast<AFPSCharacter>(OtherActor);

	if (attacker) {
		GetWorldTimerManager().ClearTimer(CaptureTimer);
		GetWorldTimerManager().SetTimer(CaptureTimer, this, &ACaptureSphere::Capture, CaptureRateUp, true, 0.0f);
	}
}

void ACaptureSphere::OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor) {
	
	if (OverlappedActor) {
		GetWorldTimerManager().ClearTimer(CaptureTimer);
		GetWorldTimerManager().SetTimer(CaptureTimer, this, &ACaptureSphere::UnCapture, CaptureRateDown, true, 0.0f);
	}
}

void ACaptureSphere::Capture() {

	if (CaptureStatus < CaptureDefault) {
		CaptureStatus += 5.f;
	}
	else if (CaptureStatus >= CaptureDefault) {
		bCapture = true;
		GetWorldTimerManager().ClearTimer(CaptureTimer);
	}
	
}

void ACaptureSphere::UnCapture() {

	if (CaptureStatus < CaptureDefault && CaptureStatus >= 5) {
		CaptureStatus -= 5.f;
	}
}

bool ACaptureSphere::GetCaptureStatus() {
	return bCapture;
}

void ACaptureSphere::EndRound() {	

	AFPSGameMode * GM = Cast<AFPSGameMode>(GetWorld()->GetAuthGameMode());
	if (GM) {
		GM->OnCapture();
	}
	CaptureStatus = 0.f;
}