// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSGameState.h"
#include "FPSPlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "FPSGameMode.h"


AFPSGameState::AFPSGameState() {

	TeamAScore = 0;
	TeamBScore = 0;
	RoundTimeRep = 0;
	RoundRestartTimeRep = 0;
}

void AFPSGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {

	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AFPSGameState, TeamAScore);
	DOREPLIFETIME(AFPSGameState, TeamBScore);
	DOREPLIFETIME(AFPSGameState, RoundTimeRep);
	DOREPLIFETIME(AFPSGameState, RoundRestartTimeRep);
}


void AFPSGameState::Tick(float DeltaSeconds) {

	Super::Tick(DeltaSeconds);
	//UE_LOG(LogTemp, Log, TEXT("Player Connected : %s"), *FString::SanitizeFloat(ElapsedTime));
}

void AFPSGameState::BeginPlay() {
	Super::BeginPlay();
}


void AFPSGameState::AddScore(FString TeamName) {

	if(TeamName == "Attacker") {
		TeamAScore += 1.f;
	}
	else if (TeamName == "Defender") {
		TeamBScore += 1.f;
	}
}